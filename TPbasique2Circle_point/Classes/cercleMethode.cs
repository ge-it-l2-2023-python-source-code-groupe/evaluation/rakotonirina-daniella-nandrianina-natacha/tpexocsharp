using espacePoint;
namespace espaceCercle;
public partial class Cercle{
    public double GetPerimeter(){
            return 2 * Math.PI * rayon;
        }

        public double GetSurface(){
            return Math.PI * rayon * rayon;
        }

        public bool IsInclude(Point p){
            double distance = Math.Sqrt(Math.Pow(p.X - centre.X, 2) + Math.Pow(p.Y - centre.Y, 2));
            return distance <= rayon;
        }

        public void Display(){
            Console.WriteLine($"CERCLE({centre.X},{centre.Y},{rayon})");
        }
}