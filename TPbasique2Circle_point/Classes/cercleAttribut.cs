namespace espaceCercle;
using espacePoint;
public partial class Cercle{
        private Point centre;
        private double rayon;

        public Cercle(Point centre, double rayon){
            this.centre = centre;
            this.rayon = rayon;
        }

        public Cercle(double x,double y,double r){
            centre = new Point(0, 0);
            rayon = 0;
        }

        public Cercle(){
            Console.WriteLine("Donnez l'abscisse du centre:");
            bool a = double.TryParse(Console.ReadLine()??"",out double x );

            Console.WriteLine("Donnez l'ordonné du centre:");
            a = double.TryParse(Console.ReadLine()??"",out double y);

            Console.WriteLine("Donnez le rayon:");
            a = double.TryParse(Console.ReadLine()??"",out double r);

            centre = new Point(x, y);
            rayon = r;
        }

        
    }


