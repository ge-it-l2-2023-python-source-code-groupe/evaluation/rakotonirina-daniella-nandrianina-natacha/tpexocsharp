﻿using espaceCercle;
using espacePoint;
namespace TPbasique2Circle_point;
class Program_Principal2{
    public static void TpCercle(){
        Cercle cercle = new Cercle();
        cercle.Display();
        Console.WriteLine($"Le périmètre est : {cercle.GetPerimeter():F2}");
        Console.WriteLine($"La surface est : {cercle.GetSurface():F2}");
        Console.WriteLine("Donnez un point:");
        Console.Write("X:");
        bool b = double.TryParse(Console.ReadLine()??"",out double pointX );
        Console.Write("Y:");
        b = double.TryParse(Console.ReadLine()??"",out double pointY );
        Point point = new Point(pointX, pointY);
        point.Display();
        if (cercle.IsInclude(point))
        {
            Console.WriteLine("Le point appartient au cercle.");
        }
        else
        {
            Console.WriteLine("Le point n'appartient pas au cercle.");
        }

    }
}

