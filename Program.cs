﻿using System;
namespace TpExoCsharp;
using System;
using System.Threading;
using TPbasique1Client_compte;
using TPbasique2Circle_point;
using TPbasique3_JeuDeDes;
using TPbasique4Horloge_module;


class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Bonjour! Bienvenue sur le programme de TP C#!");

        while (true){
           Console.WriteLine("Le menu est comme suit: ");
           Console.WriteLine("""
           1) TP Basique 1: Client_Compte
           2) TP Basique 2: Circle_Point
           3) TP Basique 3: Jeu de dés
           4) TP Basique 4: Horloge_Module
           5)Quitter

           """);
           string choix= Console.ReadLine()??"";
           switch (choix)
           {
            case "1":
            TPbasique1Client_compte.Program_Principal1.TPbasique1Client_compte();
            Console.WriteLine("Revenir au menu");
            Console.ReadKey();
            break;
            case "2":
            TPbasique2Circle_point.Program_Principal2.TpCercle();
              Console.WriteLine("Revenir au menu");
              Console.ReadKey();
            break;
            case "3":
            TPbasique3_JeuDeDes.ProgramPrincipal3.JeuDeDes();
              Console.WriteLine("Revenir au menu");
              Console.ReadKey();
            break;
            case "4":
             TPbasique4Horloge_module.Program_Principal4.TpHorloge_module();
               Console.WriteLine("Revenir  au menu");
               Console.ReadKey();
             break;
           
           }
            if(choix=="5"){
                  Console.ReadKey();
                    Console.WriteLine("Toucher pour quitter");
            
                break;
            }
        }
    }
}
