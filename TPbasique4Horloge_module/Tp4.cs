﻿

using System;
using System.Security.Cryptography;
using Alarme;
using Heure;

namespace TPbasique4Horloge_module
{

    class Program_Principal4
    {
        internal static void TpHorloge_module()
        {
            int choix;

            do
            {
                DisplayMenu();
                choix = GetChoix();

                switch (choix)
                {
                    case 1:
                        AlarmApp alarmApp = new AlarmApp();
                        alarmApp.Run();
                        break;
                    case 2:
                        Heure.FuseauHoraire.RunClockApp();
                        break;

                    default:
                        break;
                }
            } while (choix != 3);

            void DisplayMenu()
            {
                Console.WriteLine("1- Alarme");
                Console.WriteLine("2- Horloge");
                Console.WriteLine("3- Quitter");

            }
            int GetChoix()
            {
                int choix;
                while (!int.TryParse(Console.ReadLine(), out choix))
                {
                    Console.WriteLine("Entrer invalide :::");
                    Console.WriteLine("Votre choix : ");
                }
                return choix;
            }



        }
    }
}
