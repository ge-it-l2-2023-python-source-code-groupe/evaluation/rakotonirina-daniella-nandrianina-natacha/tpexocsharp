using System;
using System.Collections.Generic;
namespace Alarme
{



    class AlarmApp
    {
        private List<Alarm> alarmList = new List<Alarm>();

        public void Run()
        {
            int choice;

            do
            {
                AfficherMenu();
                choice = ChoixMenu();

                switch (choice)
                {
                    case 1:
                        AlarmeActive();
                        break;
                    case 2:
                        CreerAlarme();
                        break;
                    case 3:
                        Console.WriteLine("Retour au menu principal.");
                        break;
                    default:
                        Console.WriteLine("Choix non valide. Veuillez réessayer.");
                        break;
                }

            } while (choice != 3);
        }

        private void AfficherMenu()
        {
            Console.WriteLine("Choisissez une option :");
            Console.WriteLine("1 - Voir les Alarmes actives");
            Console.WriteLine("2 - Créer une alarme");
            Console.WriteLine("3 - Retour au menu principal");
            Console.Write("Quel est votre choix ? ");
        }

        private int ChoixMenu()
        {
            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine("Entrée non valide. Veuillez entrer un nombre.");
                Console.Write("Quel est votre choix ? ");
            }
            return choice;
        }

        private void AlarmeActive()
        {
            if (alarmList.Count == 0) {
                Console.WriteLine("Il n'y a pas d'alarme active");
                Console.ReadKey();
            } else {
                Console.WriteLine("Liste des alarmes actives :");
                foreach (var alarm in alarmList)
                {
                    Console.WriteLine(alarm.ToString());
                }
            }
        }

        private void CreerAlarme()
        {
            Console.WriteLine("Info de la nouvelle alarme :");

            Console.Write("Donnez un nom de référence : ");
            string name = Console.ReadLine()??"";

            Console.Write("Heure de l'alarme (hh:mm) : ");
            string time = Console.ReadLine()??"";

            Console.Write("Périodique (y/n) : ");
            bool isPeriodic = Console.ReadLine()?.ToLower() == "y";

            Alarm newAlarm;

            if (isPeriodic) {
                Console.Write("Jours de planification (L/M/ME/J/V/S/D) : ");
                string date = Console.ReadLine()??"";
                bool a = TimeOnly.TryParse(time, out TimeOnly scheduledTime);
                newAlarm = new Alarm(name, date.Split(' ').ToList(), scheduledTime);
            } else {
                Console.Write("Date de planification (dd/mm/yyyy) : ");
                string date = Console.ReadLine() ?? "";
                bool a = DateTime.TryParse($"{date} {time}", out DateTime scheduledDate);
                newAlarm = new Alarm(name, scheduledDate);
            }
            
            alarmList.Add(newAlarm);

            Console.WriteLine("Votre nouvelle alarme est enregistrée : ");
        }
        
    }

    class Alarm
    {
        public string Name { get; set; }
        public DateTime ScheduledDate { get; set; }
        public List<string> RepetitionDay { get; set; }
        public TimeOnly ScheduledTime { get; set; }
        private bool IsPeriodic { get; set; }

        public Alarm(string name, DateTime scheduledTime)
        {
            Name = name;
            ScheduledDate = scheduledTime;
            RepetitionDay = new List<string> {};
            ScheduledTime = new TimeOnly();
            IsPeriodic = false;
        }

        public Alarm(string name, List<string> repetitionDay, TimeOnly scheduledTime) {
            Name = name;
            RepetitionDay = repetitionDay;
            ScheduledDate = new DateTime();
            ScheduledTime = scheduledTime;
            IsPeriodic = true;
        }

        public override string ToString()
        {
            string alarmInfo;
            if (IsPeriodic)
            {
                alarmInfo = $"{Name} - {ScheduledTime:hh:mm} (périodique) \n {GetDaysOfWeek()}";
            } else {
                alarmInfo = $"{Name} - {ScheduledDate:hh:mm} \n{ScheduledDate:dd MMMM yyyy}";
            }
            return alarmInfo;
        }

        private string GetDaysOfWeek()
        {
            List<string> formatedRepetitionDay = new List<string> {};

            for (int i = 0; i < RepetitionDay.Count; i++)
            {
                switch (RepetitionDay[i])
                {
                    case "L":
                        formatedRepetitionDay.Add("Lun.");
                        break;
                    case "M":
                        formatedRepetitionDay.Add("Mar.");
                        break;
                    case "ME":
                        formatedRepetitionDay.Add("Mer.");
                        break;
                    case "J":
                        formatedRepetitionDay.Add("Jeu.");
                        break;
                    case "V":
                        formatedRepetitionDay.Add("Ven.");
                        break;
                    case "S":
                        formatedRepetitionDay.Add("Sam.");
                        break;
                    case "D":
                        formatedRepetitionDay.Add("Dim.");
                        break;
                }
            }
            return string.Join(", ", formatedRepetitionDay);
        }
    }
}