using System;
using System.Collections.Generic;


namespace Heure
{
    class FuseauHoraire
    {
        public static void RunClockApp(){
            FuseauHoraire clockApp=new FuseauHoraire();
            clockApp.Debut();
        }
        private List<Clock> clockList = new List<Clock>();

        public void Debut()
        {
            int choix;

            do
            {
                DisplayMenu();
                choix = MenuChoix();

                switch (choix)
                {
                    case 1:
                        VoirHeureActuelle();
                        break;
                    case 2:
                        AjoutFuseau();
                        break;
                    case 3:
                        Console.WriteLine("Retour au menu principal.");
                        break;
                    default:
                        Console.WriteLine("Choix non valide. Veuillez réessayer.");
                        break;
                }

            } while (choix != 3);
        }

        private void DisplayMenu()
        {
            Console.WriteLine("Choisissez une option :");
            Console.WriteLine("1 - Voir les Horloges actives");
            Console.WriteLine("2 - Ajouter une horloge");
            Console.WriteLine("3 - Retour au menu principal");
            Console.Write("Quel est votre choix ? ");
        }

        private int MenuChoix()
        {
            int choix;
            while (!int.TryParse(Console.ReadLine(), out choix))
            {
                Console.WriteLine("Entrée non valide. Veuillez entrer un nombre.");
                Console.Write("Quel est votre choix ?");
            }
            return choix;
        }

        private void VoirHeureActuelle()
        {
            Console.WriteLine("Liste des horloges :");
            foreach (var clock in clockList)
            {
                Console.WriteLine(clock.ToString());
            }

            HeureActuelle();
        }

        private void HeureActuelle()
        {
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss"));
            Console.WriteLine(DateTime.Now.ToString("ddd dd MMM"));
        }

        private void AjoutFuseau()
        {
            Console.WriteLine("Choisissez une ville :");
            Console.Write("[Canada, Australie, Paris] : ");
            string city = Console.ReadLine() ?? "";

            Clock newClock = new Clock(city);
            clockList.Add(newClock);

            Console.WriteLine("Votre horloge a bien été enregistrée :)");
        }
    }

    class Clock{
        public string Ville { get; set; }
        public int Decalage{ get; set;}

        public Clock(string city)
        {
            Ville = city;
            Decalage = GetDecalage(city);
        }

        private static int GetDecalage(string city)
        {
            return city.ToLower() switch
            {
                "canada" => -7,
                "australie" => +10,
                "paris" => +1,
                _ => 0,
            };
        }

        public override string ToString()

        {
            DateTime heureLocale = DateTime.UtcNow.AddHours(Decalage);
            string timeString= heureLocale.ToString("HH:mm");
            return $"{Ville} - {timeString} (GMT{(Decalage >= 0 ? "+" : "")}{Decalage})";
        }
    }

}