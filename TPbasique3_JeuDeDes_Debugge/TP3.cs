﻿using MethodesJeu;
using HistoriqueJeu;

namespace TPbasique3_JeuDeDes {
    class ProgramPrincipal3 {
        public static void JeuDeDes() {
            Console.WriteLine("Bienvenue sur ce jeu de dés!\n");

            Joueurs.NouveauJoueur();
            bool jouer = true;
            while (jouer) {
                Console.WriteLine(@"
1) Nouvelle manche 
2) Voir l'historique des manches 
3) Quitter le programme ");

                Console.Write("\nVotre choix : ");
                string choix = Console.ReadLine() ?? "";

                switch (choix) {
                    case "1":
                        Console.WriteLine("\nNouvelle manche!");
                        Joueurs.Manche();
                        Console.ReadKey();
                        break;
                    case "2":
                        Console.WriteLine("\nHistorique des manches:");
                        if (Tour.HistoriqueTours.Equals(""))
                            Console.WriteLine("\nIl n'y a pas d'historique à afficher.");
                        else
                            Tour.AfficherHistorique();
                        Console.ReadKey();
                        break;
                    case "3":
                        Console.WriteLine("\nVous quittez le programme");
                        Console.ReadKey();
                        jouer = false;
                        break;
                    default:
                        Console.WriteLine("Il y a une erreur!!!!! ");
                        break;
                }
            }
        }
    }
}
    
















