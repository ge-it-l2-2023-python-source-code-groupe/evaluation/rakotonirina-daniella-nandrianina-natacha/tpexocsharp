using HistoriqueJeu;

namespace MethodesJeu {
    static partial class Joueurs {
        public static void NouveauJoueur() {
            nbJoueurs = 0;
            listeJoueurs = "";
            scoresJoueurs = "";
            pointsJoueurs = "";
            while (true){
                Console.Write("Entrez le NOMBRE de joueur : ");
                if (int.TryParse(Console.ReadLine(), out nbJoueurs)){
                    if (nbJoueurs >= 2){
                        break; 
                    }else{
                        Console.WriteLine("Le nombre de joueurs doit être au moins 2. Veuillez réessayer.");
                    }
                }else{
                    Console.WriteLine("Veuillez entrer un nombre entier valide. Veuillez réessayer.");
                }
            }
            for (int i = 1; i <= nbJoueurs; i++) {
                Console.Write($"\nEntrez le nom du joueur {i} : ");
                string name = Console.ReadLine() ?? "aa";

                listeJoueurs += name + ";";
                scoresJoueurs += 0 + ";";
                pointsJoueurs += 0 + ";";
            }
 
            Console.WriteLine("\nLes joueurs sont : ");

            for (int i = 0; i < nbJoueurs; i++) {
                Console.WriteLine($"- {listeJoueurs.Split(";")[i]} : 0");
            }
        }

        public static void Manche() {
            Tour tourActuel = new Tour();

            tourActuel.ListeJoueursTour = listeJoueurs;

            for (int i = 0; i < nbJoueurs; i++) {
                Console.WriteLine($" \n>>>>>>  Tour de {listeJoueurs.Split(";")[i]} ");
                bool saisieValide = false;

                string  choix ="";
                while (!saisieValide){
                    Console.WriteLine("======> Voulez vous lancer le dé ? (Y/n) ");
                    Console.Write("======> ");
                    choix = Console.ReadLine()?? "";
                    if (choix.Length > 0 && choix.ToLower()[0] == 'y'){
                        saisieValide = true;
                    }else if(choix.Length > 0 && choix.ToLower()[0]== 'n'){
                        saisieValide = true;
                    }else{
                        Console.WriteLine("Saisie invalide. Veuillez entrer 'Y' pour oui ou 'N' pour non.");
                    }
                }
                    
                        switch (choix.ToLower()[0]) {
                            case 'y':
                                LancerDe(tourActuel);
                                
                                break;
                            case 'n':
                                tourActuel.FacesJoueurs += "1;";
                                Console.WriteLine("------ Vous avez passé votre tour ------\n");
                                
                                break;
                            
                            default:
                                tourActuel.FacesJoueurs += "1";
                                break;
                        }
                    

            }

            scoresJoueurs = AjouterScores(tourActuel);
            tourActuel.ScoresJoueursTour = scoresJoueurs;

            pointsJoueurs = AjouterPoints(tourActuel);
            tourActuel.PointsJoueursTour = pointsJoueurs;

            tourActuel.AjouterHistorique();

            Console.WriteLine("\nPoints des joueurs :");

            for (int i = 0; i < nbJoueurs; i++) {
                Console.WriteLine($"- {listeJoueurs.Split(";")[i]} : {pointsJoueurs.Split(";")[i]}");
            }

            Console.WriteLine("\nScores des joueurs :");

            for (int i = 0; i < nbJoueurs; i++) {
                Console.WriteLine($"- {listeJoueurs.Split(";")[i]} : {scoresJoueurs.Split(";")[i]}");
            }
        }

        static void LancerDe(Tour tourActuel) {
            Random random = new Random();
            int randomNumber = random.Next(1, 7);

            if (randomNumber == 1) {
                Console.WriteLine("|     |\n|  *  |\n|     |");
                tourActuel.FacesJoueurs += randomNumber + ";";
            } else if (randomNumber == 2) {
                Console.WriteLine("|*    |\n|     |\n|    *|");
                tourActuel.FacesJoueurs += randomNumber + ";";
            } else if (randomNumber == 3) {
                Console.WriteLine("|*    |\n|  *  |\n|    *|");
                tourActuel.FacesJoueurs += randomNumber + ";";
            } else if (randomNumber == 4) {
                Console.WriteLine("|*   *|\n|     |\n|*   *|");
                tourActuel.FacesJoueurs += randomNumber + ";";
            } else if (randomNumber == 5) {
                Console.WriteLine("|*   *|\n|  *  |\n|*   *|");
                tourActuel.FacesJoueurs += randomNumber + ";";
            } else if (randomNumber == 6) {
                Console.WriteLine("|*   *|\n|*   *|\n|*   *|");
                tourActuel.FacesJoueurs += randomNumber + ";";
                tourActuel.NbJoueursMax++;
            }
        }

        static string AjouterPoints(Tour tourActuel) {
            string tmpListePoints = "";

            for(int i = 0; i < tourActuel.FacesJoueurs.Split(";").Length - 1; i++) {
                if (tourActuel.FacesJoueurs.Split(";")[i] == "6" && tourActuel.NbJoueursMax == 1) {
                    tmpListePoints += Convert.ToString(int.Parse(pointsJoueurs.Split(";")[i]) + 2) + ";";
                } else if (tourActuel.FacesJoueurs.Split(";")[i] == "6" && tourActuel.NbJoueursMax > 1) {
                    tmpListePoints += Convert.ToString(int.Parse(pointsJoueurs.Split(";")[i]) + 1) + ";";
                } else {
                    tmpListePoints += Convert.ToString(int.Parse(pointsJoueurs.Split(";")[i]) + 0) + ";";
                }
            }
            return tmpListePoints;
        }

        static string AjouterScores(Tour tourActuel) {
            string tmpListeScores = "";

            for(int i = 0; i < tourActuel.FacesJoueurs.Split(";").Length - 1; i++) {
                if (tourActuel.FacesJoueurs.Split(";")[i] == "6") {
                    tmpListeScores += Convert.ToString(int.Parse(scoresJoueurs.Split(";")[i]) + 1) + ";";
                } else {
                    tmpListeScores += Convert.ToString(int.Parse(scoresJoueurs.Split(";")[i]) + 0) + ";";
                }
            }
            return tmpListeScores;
        }
    }
}