namespace HistoriqueJeu {
    partial class Tour {
        public void AjouterHistorique() {
            HistoriqueTours += FormatageHistorique() + "\n";
        }

        public static void AfficherHistorique() {
            Console.WriteLine(HistoriqueTours);
        }

        private string FormatageHistorique() {
            return $@"
Manche N° {NumTour}

Date : {DateTour}

Points des joueurs :
{FormatageScoresPoints(PointsJoueursTour)}

Scores des joueurs : 
{FormatageScoresPoints(ScoresJoueursTour)}";
        }

        public string FormatageScoresPoints(string listeScoresPoints) {
            string scoresPointsFormates = "";
            for (int i = 0; i < ListeJoueursTour.Split(";").Length - 1; i++) {
                scoresPointsFormates += $"\n- {ListeJoueursTour.Split(";")[i]} : {listeScoresPoints.Split(";")[i]}";
            }
            return scoresPointsFormates;
        }
    }
}