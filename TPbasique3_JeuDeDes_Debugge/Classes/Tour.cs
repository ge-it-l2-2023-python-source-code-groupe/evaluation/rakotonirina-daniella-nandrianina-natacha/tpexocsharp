namespace HistoriqueJeu {
    partial class Tour {
        public int NumTour { get; private set; }
        private static int num = 0;
        public DateTime DateTour { get; set; }
        public string ListeJoueursTour { get; set; }
        public string ScoresJoueursTour { get; set; }
        public string PointsJoueursTour { get; set; }
        public string FacesJoueurs { get; set; }
        public int NbJoueursMax { get; set; }
        public static string HistoriqueTours = "";

        public Tour() {
            NumTour = ++num;
            DateTour = DateTime.Now;
            ListeJoueursTour = "";
            ScoresJoueursTour = "";
            PointsJoueursTour = "";
            FacesJoueurs = "";
            NbJoueursMax = 0;
        }
    }
}