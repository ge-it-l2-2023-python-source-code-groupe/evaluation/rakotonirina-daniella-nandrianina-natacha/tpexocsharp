﻿using System;
using Class_Cli;
using Class_Compte;
namespace TPbasique1Client_compte
{
    

class Program_Principal1
{
      

    internal static void TPbasique1Client_compte()
    {

        string cin, nom, prenom, tel;
        float somme = 0;
        Compte.ReinitialiserCompteur();
        
        Console.Write($"Compte 1:\nDonner Le CIN: ");
        cin = Console.ReadLine()??"";
        Console.Write($"Donner Le Nom: ");
        nom = Console.ReadLine()??"";
        Console.Write($"Donner Le Prénom: ");
        prenom = Console.ReadLine()??"";
        Console.Write($"Donner Le numéro de télephone: ");
        tel = Console.ReadLine()??"";

        Compte compte1 = new Compte(new Client(cin, nom, prenom, tel));
        Console.WriteLine($"Détails du compte:{compte1.Resumer()}");
        bool saisieMontantValide = false;
        while (!saisieMontantValide){
            Console.Write($"Donner le montant à déposer: ");
            saisieMontantValide = float.TryParse(Console.ReadLine()??"", out somme);
            if (!saisieMontantValide){
                Console.WriteLine("Saisie invalide. Veuillez entrer un montant numérique valide.");
            }
        }
        compte1.Crediter(somme);
        Console.WriteLine($"Opération bien effectuée{compte1.Resumer()}");
        
        saisieMontantValide = false;
        while (!saisieMontantValide){
            Console.Write($"Donner le montant à retirer: ");
            saisieMontantValide = float.TryParse(Console.ReadLine()??"", out somme);
            if (!saisieMontantValide){
                Console.WriteLine("Saisie invalide. Veuillez entrer un montant numérique valide.");
            }
        }
        compte1.Debiter((float)somme);
        Console.WriteLine($"Opération bien effectuée{compte1.Resumer()}");
        Console.Write($"\n\n\nCompte 2:\nDonner Le CIN: ");
        cin = Console.ReadLine()??"";
        Console.Write($"Donner Le Nom: ");
        nom = Console.ReadLine()??"";
        Console.Write($"Donner Le Prénom: ");
        prenom = Console.ReadLine()??"";
        Console.Write($"Donner Le numéro de télephone: ");
        tel = Console.ReadLine()??"";
        
        Compte compte2 = new Compte(new Client(cin, nom, prenom, tel));
        Console.WriteLine($"Détails du compte:{compte2.Resumer()}");

        Console.WriteLine($"Crediter le compte {compte2.Code} à partir du compte {compte1.Code}");
        saisieMontantValide = false;
        while (!saisieMontantValide){
            Console.Write($"Donner le montant à retirer: ");
            saisieMontantValide = float.TryParse(Console.ReadLine()??"", out somme);
            if (!saisieMontantValide){
                Console.WriteLine("Saisie invalide. Veuillez entrer un montant numérique valide.");
            }
        }
        compte2.Crediter((float)somme, compte1);
        Console.WriteLine($"Opération bien effectuée");
        
        Console.WriteLine($"Débiter le compte {compte2.Code} et créditer le compte {compte1.Code}");
        saisieMontantValide = false;
        while (!saisieMontantValide){
            Console.Write($"Donner le montant à retirer: ");
            saisieMontantValide = float.TryParse(Console.ReadLine()??"", out somme);
            if (!saisieMontantValide){
                Console.WriteLine("Saisie invalide. Veuillez entrer un montant numérique valide.");
            }
        }
        compte2.Debiter((float)somme, compte1);
        Console.Write($"Opération bien effectuée");

        Console.WriteLine($"{compte1.Resumer()}{compte2.Resumer()}");
        Console.Write($"\n\n\n{Compte.NombreComptesCrees()}\n\n\n");
        Console.ReadKey();
    }
}
}