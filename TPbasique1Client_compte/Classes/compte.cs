using System;
using Class_Cli;

namespace Class_Compte{
    class Compte
    {
        private float solde;
        public float Solde { get { return solde; } }
        private uint code;
        public uint Code { get { return code; } }
        public Client PropriCompte { get; set; }

        private static uint compteur = 0;

        public string Resumer()
        {
            return $@"
        ************************
        Numéro de Compte: {Code}
        Solde de compte: {Solde}
        Propriétaire du compte:
        {PropriCompte.Afficher()}
        ************************";
        }
        public static void ReinitialiserCompteur(){
            compteur = 0;
        }

        public static string NombreComptesCrees()
        {
            return $"Le nombre de comptes crées: {compteur}";
        }

        public void Debiter(float montant, Compte compteClient)
        {
            Debiter(montant);
            compteClient.Crediter(montant);
        }

        public void Debiter(float montant)
        {
            solde -= montant;
        }

        public void Crediter(float montant, Compte compteClient)
        {
            Crediter(montant);
            compteClient.Debiter(montant);
        }

        public void Crediter(float montant)
        {
            solde += montant;
        }

        public Compte(Client client, float montant)
        {
            PropriCompte = client;
            solde = montant;
            code = ++compteur;
        }

        public Compte(Client client)
        {
            PropriCompte = client;
            solde = default;
            code = ++compteur;
        }
    }
}